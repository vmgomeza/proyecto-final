/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Figuras;

import javax.media.opengl.GL;
import javax.media.opengl.glu.GLU;
import javax.media.opengl.glu.GLUquadric;


public class DiscoParcial {
    
    GL gl;
    GLU glu;
    GLUquadric quad;
    float alto, ancho, profundidad;
    float rx, ry, rz;
    float r,b,g;

    public DiscoParcial(GL gl, GLU glu, GLUquadric quad, float alto, float ancho, float profundidad, float rx, float ry, float rz) {
        this.gl = gl;
        this.glu = glu;
        this.quad = quad;
        this.alto = alto;
        this.ancho = ancho;
        this.profundidad = profundidad;
        this.rx = rx;
        this.ry = ry;
        this.rz = rz;
    }
    

    public void DibujaDisco(){
        
        quad =glu.gluNewQuadric();
        glu.gluQuadricDrawStyle(quad,GLU.GLU_FILL);
        
        gl.glPushMatrix();
        
        gl.glColor3f(this.r, this.g, this.b);
        gl.glRotatef(this.rx, 1, 0, 0);
        gl.glRotatef(this.ry, 0, 1, 0);
        gl.glRotatef(this.rz, 0, 0, 1);
        gl.glScalef(this.ancho, this.alto, this.profundidad);
        glu.gluPartialDisk(quad, 0.8,0.9, 50, 50, 90, 180);
        gl.glEnd();
        
        gl.glPopMatrix();
    }

    public void setR(float r) {
        this.r = r;
    }

    public void setB(float b) {
        this.b = b;
    }

    public void setG(float g) {
        this.g = g;
    }
    
    

    
}
