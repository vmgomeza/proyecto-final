/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Figuras;

import com.sun.opengl.util.GLUT;
import javax.media.opengl.GL;


public class Cono {

 GL gl;
    GLUT glut;
    float alto, ancho, profundidad;
    float rx, ry, rz;
    float r,b,g;

    public Cono(GL gl, GLUT glut, float alto, float ancho, float profundidad, float rx, float ry, float rz) {
        this.gl = gl;
        this.glut = glut;
        this.alto = alto;
        this.ancho = ancho;
        this.profundidad = profundidad;
        this.rx = rx;
        this.ry = ry;
        this.rz = rz;
    }
    
    


    

    public void DibujaCono(){
        
        gl.glPushMatrix();
        gl.glColor3f(this.r, this.g, this.b);
        gl.glRotatef(this.rx, 1, 0, 0);
        gl.glRotatef(this.ry, 0, 1, 0);
        gl.glRotatef(this.rz, 0, 0, 1);
        gl.glScalef(this.ancho, this.alto, this.profundidad);
        glut.glutWireCone(1, 1, 80, 80);
        gl.glEnd();
        
        gl.glPopMatrix();
    }

    public void setR(float r) {
        this.r = r;
    }

    public void setB(float b) {
        this.b = b;
    }

    public void setG(float g) {
        this.g = g;
    }
    
    

    
}
