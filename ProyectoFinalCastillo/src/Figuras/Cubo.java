/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Figuras;

import javax.media.opengl.GL;

/**
 *
 * @author Vale
 */
public class Cubo {
    
    GL gl;
    float alto, ancho, profundidad;
    float rx, ry, rz;
    float r,b,g;

    public Cubo(GL gl,float alto, float ancho, float profundidad, float rx, float ry, float rz) {
        this.gl = gl;
        this.alto = alto;
        this.ancho = ancho;
        this.profundidad = profundidad;
        this.rx = rx;
        this.ry = ry;
        this.rz = rz;
    }

    public void setR(float r) {
        this.r = r;
    }

    public void setB(float b) {
        this.b = b;
    }

    public void setG(float g) {
        this.g = g;
    }

     
    //Cubo caras superiores mas claras (gradas)
    public void DibujarCubo(){
        
        gl.glPushMatrix();

        
        gl.glColor3f(this.r-0.1f, this.g-0.1f, this.b-0.1f);
        gl.glRotatef(this.rx, 1, 0, 0);
        gl.glRotatef(this.ry, 0, 1, 0);
        gl.glRotatef(this.rz, 0, 0, 1);
        gl.glScaled(this.ancho,this.alto,this.profundidad);

        
        gl.glBegin(GL.GL_QUADS);
        //Cara Frontal
        gl.glVertex3f(-1, 1, -1);
        gl.glVertex3f(1, 1, -1);
        gl.glVertex3f(1, -1, -1);
        gl.glVertex3f(-1, -1, -1);
        
        //Cara Trasera
        gl.glVertex3f(1, 1, 1);
        gl.glVertex3f(-1, 1, 1);
        gl.glVertex3f(-1, -1, 1);
        gl.glVertex3f(1, -1, 1);

        //Cara Lateral Izquierda
        gl.glVertex3f(-1, 1, 1);
        gl.glVertex3f(-1, 1, -1);
        gl.glVertex3f(-1, -1, -1);
        gl.glVertex3f(-1, -1, 1);

        //Cara Lateral Derecha
        gl.glVertex3f(1, 1, -1);
        gl.glVertex3f(1, 1, 1);
        gl.glVertex3f(1, -1, 1);
        gl.glVertex3f(1, -1, -1);

        gl.glColor3f(r, g, b);
        //Cara Superior
        gl.glVertex3f(1, 1, -1);
        gl.glVertex3f(1, 1, 1);
        gl.glVertex3f(-1, 1, 1);
        gl.glVertex3f(-1, 1, -1);

        //Cara Inferior
        gl.glVertex3f(1, -1, -1);
        gl.glVertex3f(1, -1, 1);
        gl.glVertex3f(-1, -1, 1);
        gl.glVertex3f(-1, -1, -1);
        
        gl.glEnd();

        
        gl.glPopMatrix();
    }

    //cubo caras laterales (2) columnas
    public void DibujarCubo2(){
        
        gl.glPushMatrix();
        gl.glColor3f(r-0.1f, g-0.1f, b-0.1f);
        gl.glRotatef(rx, 1, 0, 0);
        gl.glRotatef(ry, 0, 1, 0);
        gl.glRotatef(rz, 0, 0, 1);
        gl.glScaled(ancho,alto,profundidad);

        
        gl.glBegin(GL.GL_QUADS);
        //Cara Frontal
        gl.glVertex3f(-1, 1, -1);
        gl.glVertex3f(1, 1, -1);
        gl.glVertex3f(1, -1, -1);
        gl.glVertex3f(-1, -1, -1);
        
        //Cara Trasera
        gl.glVertex3f(1, 1, 1);
        gl.glVertex3f(-1, 1, 1);
        gl.glVertex3f(-1, -1, 1);
        gl.glVertex3f(1, -1, 1);

        gl.glColor3f(r, g, b);
        //Cara Lateral Izquierda
        gl.glVertex3f(-1, 1, 1);
        gl.glVertex3f(-1, 1, -1);
        gl.glVertex3f(-1, -1, -1);
        gl.glVertex3f(-1, -1, 1);

        //Cara Lateral Derecha
        gl.glVertex3f(1, 1, -1);
        gl.glVertex3f(1, 1, 1);
        gl.glVertex3f(1, -1, 1);
        gl.glVertex3f(1, -1, -1);

        
        //Cara Superior
        gl.glVertex3f(1, 1, -1);
        gl.glVertex3f(1, 1, 1);
        gl.glVertex3f(-1, 1, 1);
        gl.glVertex3f(-1, 1, -1);

        //Cara Inferior
        gl.glVertex3f(1, -1, -1);
        gl.glVertex3f(1, -1, 1);
        gl.glVertex3f(-1, -1, 1);
        gl.glVertex3f(-1, -1, -1);
        
        gl.glEnd();

        
        gl.glPopMatrix();
    }
    
    //cubo 1 cara pared derecha
    public void DibujarCuboD(){
        
        gl.glPushMatrix();

        gl.glColor3f(r-0.1f, g-0.1f, b-0.1f);
        gl.glRotatef(rx, 1, 0, 0);
        gl.glRotatef(ry, 0, 1, 0);
        gl.glRotatef(rz, 0, 0, 1);
        gl.glScaled(ancho,alto,profundidad);

        
        gl.glBegin(GL.GL_QUADS);
       
        //Cara Frontal
        gl.glVertex3f(-1, 1, -1);
        gl.glVertex3f(1, 1, -1);
        gl.glVertex3f(1, -1, -1);
        gl.glVertex3f(-1, -1, -1);
        
        
        gl.glColor3f(r, g, b);
        //Cara Trasera
        gl.glVertex3f(1, 1, 1);
        gl.glVertex3f(-1, 1, 1);
        gl.glVertex3f(-1, -1, 1);
        gl.glVertex3f(1, -1, 1);
        
        
        
        //Cara Lateral Izquierda
        gl.glVertex3f(-1, 1, 1);
        gl.glVertex3f(-1, 1, -1);
        gl.glVertex3f(-1, -1, -1);
        gl.glVertex3f(-1, -1, 1);

        //Cara Lateral Derecha
        gl.glVertex3f(1, 1, -1);
        gl.glVertex3f(1, 1, 1);
        gl.glVertex3f(1, -1, 1);
        gl.glVertex3f(1, -1, -1);

        
        //Cara Superior
        gl.glVertex3f(1, 1, -1);
        gl.glVertex3f(1, 1, 1);
        gl.glVertex3f(-1, 1, 1);
        gl.glVertex3f(-1, 1, -1);

        //Cara Inferior
        gl.glVertex3f(1, -1, -1);
        gl.glVertex3f(1, -1, 1);
        gl.glVertex3f(-1, -1, 1);
        gl.glVertex3f(-1, -1, -1);
        
        gl.glEnd();

        
        gl.glPopMatrix();
    }
   
    //cubo 1 cara pared izquierda
    public void DibujarCuboI(){
        
        gl.glPushMatrix();

        gl.glColor3f(r-0.1f, g-0.1f, b-0.1f);
        gl.glRotatef(rx, 1, 0, 0);
        gl.glRotatef(ry, 0, 1, 0);
        gl.glRotatef(rz, 0, 0, 1);
        gl.glScaled(ancho,alto,profundidad);

        
        gl.glBegin(GL.GL_QUADS);
       
       
        //Cara Trasera
        gl.glVertex3f(1, 1, 1);
        gl.glVertex3f(-1, 1, 1);
        gl.glVertex3f(-1, -1, 1);
        gl.glVertex3f(1, -1, 1);
        
         gl.glColor3f(r, g, b);
        //Cara Frontal
        gl.glVertex3f(-1, 1, -1);
        gl.glVertex3f(1, 1, -1);
        gl.glVertex3f(1, -1, -1);
        gl.glVertex3f(-1, -1, -1);
        
        
        
        
        
        
        //Cara Lateral Izquierda
        gl.glVertex3f(-1, 1, 1);
        gl.glVertex3f(-1, 1, -1);
        gl.glVertex3f(-1, -1, -1);
        gl.glVertex3f(-1, -1, 1);

        //Cara Lateral Derecha
        gl.glVertex3f(1, 1, -1);
        gl.glVertex3f(1, 1, 1);
        gl.glVertex3f(1, -1, 1);
        gl.glVertex3f(1, -1, -1);

        
        //Cara Superior
        gl.glVertex3f(1, 1, -1);
        gl.glVertex3f(1, 1, 1);
        gl.glVertex3f(-1, 1, 1);
        gl.glVertex3f(-1, 1, -1);

        //Cara Inferior
        gl.glVertex3f(1, -1, -1);
        gl.glVertex3f(1, -1, 1);
        gl.glVertex3f(-1, -1, 1);
        gl.glVertex3f(-1, -1, -1);
        
        gl.glEnd();

        
        gl.glPopMatrix();
    }
    
    //cubo 1 cara pared trasera
     public void DibujarCuboT(){
        
        gl.glPushMatrix();

        gl.glColor3f(r-0.1f, g-0.1f, b-0.1f);
        gl.glRotatef(rx, 1, 0, 0);
        gl.glRotatef(ry, 0, 1, 0);
        gl.glRotatef(rz, 0, 0, 1);
        gl.glScaled(ancho,alto,profundidad);

        
        gl.glBegin(GL.GL_QUADS);
       
       //Cara Lateral Izquierda
        gl.glVertex3f(-1, 1, 1);
        gl.glVertex3f(-1, 1, -1);
        gl.glVertex3f(-1, -1, -1);
        gl.glVertex3f(-1, -1, 1);
        
         gl.glColor3f(r, g, b);
        //Cara Trasera
        gl.glVertex3f(1, 1, 1);
        gl.glVertex3f(-1, 1, 1);
        gl.glVertex3f(-1, -1, 1);
        gl.glVertex3f(1, -1, 1);
        
        //Cara Frontal
        gl.glVertex3f(-1, 1, -1);
        gl.glVertex3f(1, 1, -1);
        gl.glVertex3f(1, -1, -1);
        gl.glVertex3f(-1, -1, -1);
        
        

        //Cara Lateral Derecha
        gl.glVertex3f(1, 1, -1);
        gl.glVertex3f(1, 1, 1);
        gl.glVertex3f(1, -1, 1);
        gl.glVertex3f(1, -1, -1);

        
        //Cara Superior
        gl.glVertex3f(1, 1, -1);
        gl.glVertex3f(1, 1, 1);
        gl.glVertex3f(-1, 1, 1);
        gl.glVertex3f(-1, 1, -1);

        //Cara Inferior
        gl.glVertex3f(1, -1, -1);
        gl.glVertex3f(1, -1, 1);
        gl.glVertex3f(-1, -1, 1);
        gl.glVertex3f(-1, -1, -1);
        
        gl.glEnd();

        
        gl.glPopMatrix();
    }
    
    //cubo 1 cara pared frontal
     public void DibujarCuboF(){
        
        gl.glPushMatrix();

        gl.glColor3f(r-0.1f, g-0.1f, b-0.1f);
        gl.glRotatef(rx, 1, 0, 0);
        gl.glRotatef(ry, 0, 1, 0);
        gl.glRotatef(rz, 0, 0, 1);
        gl.glScaled(ancho,alto,profundidad);

        
        gl.glBegin(GL.GL_QUADS);
       
        //Cara Lateral Derecha
        gl.glVertex3f(1, 1, -1);
        gl.glVertex3f(1, 1, 1);
        gl.glVertex3f(1, -1, 1);
        gl.glVertex3f(1, -1, -1);
        
         gl.glColor3f(r, g, b);
        //Cara Trasera
        gl.glVertex3f(1, 1, 1);
        gl.glVertex3f(-1, 1, 1);
        gl.glVertex3f(-1, -1, 1);
        gl.glVertex3f(1, -1, 1);
        
        //Cara Frontal
        gl.glVertex3f(-1, 1, -1);
        gl.glVertex3f(1, 1, -1);
        gl.glVertex3f(1, -1, -1);
        gl.glVertex3f(-1, -1, -1);
        
        //Cara Lateral Izquierda
        gl.glVertex3f(-1, 1, 1);
        gl.glVertex3f(-1, 1, -1);
        gl.glVertex3f(-1, -1, -1);
        gl.glVertex3f(-1, -1, 1);
        
        //Cara Superior
        gl.glVertex3f(1, 1, -1);
        gl.glVertex3f(1, 1, 1);
        gl.glVertex3f(-1, 1, 1);
        gl.glVertex3f(-1, 1, -1);

        //Cara Inferior
        gl.glVertex3f(1, -1, -1);
        gl.glVertex3f(1, -1, 1);
        gl.glVertex3f(-1, -1, 1);
        gl.glVertex3f(-1, -1, -1);
        
        gl.glEnd();

        
        gl.glPopMatrix();
    }
    
   
}
