
package Figuras;

import javax.media.opengl.GL;

public class Piramide {
    
    GL gl;
    float alto, ancho, profundidad;
    float rx, ry, rz;
    float r,b,g;

    public Piramide(GL gl, float alto, float ancho, float profundidad, float rx, float ry, float rz) {
        this.gl = gl;
        this.alto = alto;
        this.ancho = ancho;
        this.profundidad = profundidad;
        this.rx = rx;
        this.ry = ry;
        this.rz = rz;
    }

    public void setR(float r) {
        this.r = r;
    }

    public void setB(float b) {
        this.b = b;
    }

    public void setG(float g) {
        this.g = g;
    }

    

    public void DibujarPiramide() {

        gl.glPushMatrix();

        gl.glColor3f(this.r, this.g, this.b);
        gl.glRotatef(this.rx, 1, 0, 0);
        gl.glRotatef(this.ry, 0, 1, 0);
        gl.glRotatef(this.rz, 0, 0, 1);
        gl.glScalef(this.ancho, this.alto, this.profundidad);

        gl.glBegin(GL.GL_QUADS);
        //BASE
        gl.glVertex3f(-1, -1, -1);
        gl.glVertex3f(1, -1, -1);
        gl.glVertex3f(1, -1, 1);
        gl.glVertex3f(-1, -1, 1);
        gl.glEnd();

        gl.glBegin(GL.GL_TRIANGLES);
        //Cara Lateral Derecha
        gl.glVertex3f(-1, -1, 1);
        gl.glVertex3f(0, 1, 0);
        gl.glVertex3f(-1, -1, -1);

        //Cara Lateral Izquierda
        gl.glVertex3f(1, -1, 1);
        gl.glVertex3f(0, 1, 0);
        gl.glVertex3f(1, -1, -1);

        //Cara  atras
        gl.glColor3f(this.r + 0.2f, this.g + 0.2f, this.b + 0.2f);
        gl.glVertex3f(1, -1, -1);
        gl.glVertex3f(0, 1, 0);
        gl.glVertex3f(-1, -1, -1);

        //Cara  delante
        gl.glVertex3f(1, -1, 1);
        gl.glVertex3f(0, 1, 0);
        gl.glVertex3f(-1, -1, 1);
        gl.glEnd();

        gl.glPopMatrix();
    }

}

    
    

