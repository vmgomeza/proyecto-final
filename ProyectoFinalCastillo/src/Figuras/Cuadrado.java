
package Figuras;

import javax.media.opengl.GL;

/**
 *
 * @author Vale
 */
public class Cuadrado {
    
    GL gl;
    float x, y, z;
    float alto, ancho;
    float rx, ry, rz;
    float red,green,blue;

    public Cuadrado(GL gl, float x, float y, float z, float alto, float ancho, float rx, float ry, float rz, float red, float green, float blue) {
        this.gl = gl;
        this.x = x;
        this.y = y;
        this.z = z;
        this.alto = alto;
        this.ancho = ancho;
        this.rx = rx;
        this.ry = ry;
        this.rz = rz;
        this.red = red;
        this.green = green;
        this.blue = blue;
    }
    
    public void DibujarCuadrado(){
        
        gl.glPushMatrix();

        gl.glTranslated(x, y, z);
        gl.glRotatef(rx, 1, 0, 0);
        gl.glRotatef(ry, 0, 1, 0);
        gl.glRotatef(rz, 0, 0, 1);

        gl.glColor3f(red, green, blue);
          gl.glBegin(GL.GL_QUADS);
            gl.glVertex3f(x,y, 0.0f);  // Top Left
            gl.glVertex3f(x+ancho,y, 0.0f);   // Top Right
            gl.glVertex3f(x+ancho,y+alto, 0.0f);  // Bottom Right
            gl.glVertex3f(x,y+alto, 0.0f); // Bottom Left
        gl.glEnd();
        
        gl.glPopMatrix();
    }

   
}
