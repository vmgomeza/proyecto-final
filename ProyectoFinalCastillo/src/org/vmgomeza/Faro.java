
package org.vmgomeza;

import Figuras.*;
import javax.media.opengl.GL;


public class Faro {
    
    GL gl;
    Cubo palo,l1,l2;
    Piramide foco,foco2;

    public Faro(GL gl) {
        this.gl = gl;
        this.palo = new Cubo(gl, 4, 0.7f, 0.7f, 90, 0, 0);
        this.l1 = new Cubo(gl, 0.5f, 0.5f, 0.5f, 0, 0, 0);
        this.l2 = new Cubo(gl, 0.5f, 0.5f, 0.5f, 0, 45, 0);
        this.foco = new Piramide(gl, 1, 1, 1, 180, 0, 0);
        this.foco2 = new Piramide(gl, 1, 1, 1, 180, 45, 0);
    }
    
    public void dibujarFaro(){
        
        palo.setR(0.4382f);
        palo.setG(0.0728f);
        palo.setB(0.56f);
        gl.glPushMatrix();
        gl.glTranslatef(0,0.2f,0);
        gl.glRotated(90, 1, 0, 0);
        gl.glScaled(0.1f,0.1f,0.2f);
        palo.DibujarCubo2();
        gl.glPopMatrix();
        
        l1.setR(1f);
        l1.setG(0.9003f);
        l1.setB(0.54f);
        gl.glPushMatrix();
        gl.glTranslatef(0,1f,0);
        gl.glRotated(90, 1, 0, 0);
        gl.glScaled(0.3f,0.3f,0.3f);
        l1.DibujarCubo();
        gl.glPopMatrix();
        
        l2.setR(0.9493f);
        l2.setG(1f);
        l2.setB(0.24f);
        gl.glPushMatrix();
        gl.glTranslatef(0,1f,0);
        gl.glRotated(180, 1, 0, 0);
        gl.glScaled(0.3f,0.3f,0.3f);
        l2.DibujarCubo();
        gl.glPopMatrix();
        
        foco.setR(0.7603f);
        foco.setG(0.0384f);
        foco.setB(0.96f);
        gl.glPushMatrix();
        gl.glTranslatef(0,1.3f,0);
        gl.glRotated(180, 1, 0, 0);
        gl.glScaled(0.2f,0.2f,0.2f);
        foco.DibujarPiramide();
        gl.glPopMatrix();
        foco2.setR(0.6915f);
        foco2.setG(0.48f);
        foco2.setB(0.75f);
        gl.glPushMatrix();
        gl.glTranslatef(0,1.3f,0);
        gl.glRotated(180, 1, 0, 0);
        gl.glScaled(0.2f,0.2f,0.2f);
        foco2.DibujarPiramide();
        gl.glPopMatrix();
        
        
    }
    
    
    
    
    
}
