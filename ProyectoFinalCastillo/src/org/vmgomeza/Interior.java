/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.vmgomeza;

import Figuras.Cono;
import Figuras.Cuadrado;
import Figuras.Cubo;
import Figuras.Esfera;
import com.sun.opengl.util.GLUT;
import javax.media.opengl.GL;


public class Interior {
    
    GLUT glut;
    GL gl;
    Cubo c1,c2,asiento,asiento2;
    Cuadrado cua1;
    Esfera e;
    Cono c;
    

    public Interior(GL gl,GLUT glut) {
        this.gl = gl;
        this.glut = glut;
        this.cua1 = new Cuadrado(gl, -0.4f, -0.25f, -5.5f, 5.8f, 2f, 90, 0, 0, 0.043f,0.86f,0.7783f);
        this.c1 = new Cubo(gl, 1, 1, 1, 0, 0, 0);
        this.c2 = new Cubo(gl, 0.5f, 0.5f, 0.5f, 0, 0, 0);
        this.e = new Esfera(gl, glut, 0.8f, 0.8f, 0.8f, 0, 0, 0);
        this.c = new Cono(gl, glut, 0.5f, 0.5f, 0.5f, -180, 0, 0);
        this.asiento = new Cubo(gl, 0.2f,2.5f,2.7f, 0, 0, 0);
        this.asiento2 = new Cubo(gl, 0.5f, 0.5f, 2.5f, 0,180,0);
    }
    
    public void DibujarInterior(){
        
        cua1.DibujarCuadrado();
        
        //Lamparas
        c1.setR(0.1466f);
        c1.setG(0.0188f);
        c1.setB(0.47f);
        
        //1erEstante Izquierdo
        for (float i = 0; i < 5; i+=0.8f) {
         gl.glPushMatrix();
        gl.glTranslatef(-0.8f,-0.2f,(-1)*i);
        gl.glRotated(90, 1, 0, 0);
        gl.glScaled(0.1f,0.1f,0.1f);
        c1.DibujarCubo();
        gl.glPopMatrix();
        }
        //2do Estante
        c2.setR(0.2896f);
        c2.setG(0.0285f);
        c2.setB(0.95f);
        for (float i = 0; i < 5; i+=0.8f) {
         gl.glPushMatrix();
        gl.glTranslatef(-0.8f,-0.1f,(-1)*i);
        gl.glRotated(90, 1, 0, 0);
        gl.glScaled(0.15f,0.15f,0.15f);
        c2.DibujarCubo();
        gl.glPopMatrix();
        }
        
        e.setR(0.95f);
        e.setG(0.52f);
        e.setB(0.0285f);
        for (float i = 0; i < 5; i+=0.8f) {
         gl.glPushMatrix();
        gl.glTranslatef(-0.8f,0.05f,(-1)*i);
        gl.glRotated(90, 1, 0, 0);
        gl.glScaled(0.1f,0.1f,0.1f);
        e.DibujaEsfera();
        gl.glPopMatrix();
        }
        
        c.setR(1f);
        c.setG(0.9513f);
        c.setB(0.27f);
        for (float i = 0; i < 5; i+=0.8f) {
         gl.glPushMatrix();
        gl.glTranslatef(-0.8f,0.05f,(-1)*i);
        gl.glRotated(90, 1, 0, 0);
        gl.glScaled(0.2f,0.2f,0.2f);
        c.DibujaCono();
        gl.glPopMatrix();
        }
        
        
        
        //1erEstante Derecho
        for (float i = 0; i < 5; i+=0.8f) {
         gl.glPushMatrix();
        gl.glTranslatef(1.2f,-0.2f,(-1)*i);
        gl.glRotated(90, 1, 0, 0);
        gl.glScaled(0.1f,0.1f,0.1f);
        c1.DibujarCubo();
        gl.glPopMatrix();
        }
        //2do Estante
        for (float i = 0; i < 5; i+=0.8f) {
         gl.glPushMatrix();
        gl.glTranslatef(1.2f,-0.1f,(-1)*i);
        gl.glRotated(90, 1, 0, 0);
        gl.glScaled(0.15f,0.15f,0.15f);
        c2.DibujarCubo();
        gl.glPopMatrix();
        }
        for (float i = 0; i < 5; i+=0.8f) {
         gl.glPushMatrix();
        gl.glTranslatef(1.2f,0.05f,(-1)*i);
        gl.glRotated(90, 1, 0, 0);
        gl.glScaled(0.1f,0.1f,0.1f);
        e.DibujaEsfera();
        gl.glPopMatrix();
        }
        for (float i = 0; i < 5; i+=0.8f) {
        gl.glPushMatrix();
        gl.glTranslatef(1.2f,0.05f,(-1)*i);
        gl.glRotated(90, 1, 0, 0);
        gl.glScaled(0.2f,0.2f,0.2f);
        c.DibujaCono();
        gl.glPopMatrix();
        }
        
        //Trono
        
        asiento.setR(0.87f);
        asiento.setG(0.8126f);
        asiento.setB(0.0087f);

        gl.glPushMatrix();
        gl.glTranslatef(0.2f,0.25f,-5.6f);
        gl.glRotated(90, 1, 0, 0);
        gl.glScaled(0.2f,0.1f,0.22f);
        asiento.DibujarCuboF();
        gl.glPopMatrix();
        
        asiento.setR(0.87f);
        asiento.setG(0.3676f);
        asiento.setB(0.0087f);
        gl.glPushMatrix();
        gl.glTranslatef(0.2f,0.25f,-5.4f);
        gl.glRotated(90, 1, 0, 0);
        gl.glScaled(0.1f,0.2f,0.2f);
        asiento.DibujarCuboF();
        gl.glPopMatrix();
        
        asiento2.setR(0.38f);
        asiento2.setG(0.165f);
        asiento2.setB(0.0114f);
        gl.glPushMatrix();
        gl.glTranslatef(0.2f,0.15f,-5.2f);
        gl.glRotated(90, 1, 0, 0);
        gl.glScaled(0.1f,0.2f,0.2f);
        asiento2.DibujarCuboF();
        gl.glPopMatrix();
        
        gl.glPushMatrix();
        gl.glTranslatef(0f,0.15f,-5f);
        gl.glRotated(90, 1, 0, 0);
        gl.glScaled(0.1f,0.1f,0.25f);
        asiento2.DibujarCuboF();
        gl.glPopMatrix();
        
        gl.glPushMatrix();
        gl.glTranslatef(0.4f,0.15f,-5f);
        gl.glRotated(90, 1, 0, 0);
        gl.glScaled(0.1f,0.1f,0.25f);
        asiento2.DibujarCuboF();
        gl.glPopMatrix();
        
        
        
        
       
        
    }
    
}
