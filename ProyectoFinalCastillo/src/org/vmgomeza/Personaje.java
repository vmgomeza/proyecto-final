/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.vmgomeza;

import Figuras.*;
import com.sun.opengl.util.GLUT;
import javax.media.opengl.GL;


public class Personaje {
    
    GL gl;
    GLUT glut;
    Esfera e;
    Cubo c1,c2;
    Piramide p2;
    Cono con1;
    float x, y, z;
    float ancho, alto, profundidad;
    float rx,ry,rz;
    
    

    public Personaje(GL gl, GLUT glut, float x, float y, float z, float ancho, float alto, float profundidad,float rx, float ry,float rz) {
        this.gl = gl;
        this.glut = glut;
        this.x = x;
        this.y = y;
        this.z = z;
        this.ancho = ancho;
        this.alto = alto;
        this.profundidad = profundidad;
        this.rx = rx;
        this.ry = ry;
        this.rz = rz;
        this.e = new Esfera(gl, glut, 1, 1, 1, 90, 0, 0);
        this.c1 = new Cubo(gl, 0.5f, 1, 0.5f, 0, 90, 0);
        this.c2 = new Cubo(gl, 2, 0.5f, 0.5f, 0, 90, 0);
        this.p2 = new Piramide(gl, 1f, 1f, 1f, 0, 0, 0);
        this.con1 = new Cono(gl, glut, 0.5f, 0.5f, 2f, -90, 0, 0);
    }

    public void dibujarPersonaje(){
        
        gl.glPushMatrix();
        
        gl.glTranslatef(this.x, this.y, this.z);
        gl.glRotatef(rx,1, 0, 0);
        gl.glRotatef(ry, 0, 1, 0);
        gl.glRotatef(rz, 0, 0, 1);
        gl.glScalef(this.ancho, this.alto, this.profundidad);
       
        personaje();
        
        gl.glPopMatrix();
        
        
    }
    
    public void personaje(){
        
        gl.glPushMatrix();
        //Piernas
        c1.setR(1);
        c1.setG(1);
        c1.setB(1);
        gl.glPushMatrix();
        gl.glTranslatef(0,0,0);
        gl.glRotated(0, 1, 0, 0);
        gl.glScaled(0.1f,0.1f,0.1f);
        c1.DibujarCuboI();
        gl.glPopMatrix();
        
        c2.setR(1);
        c2.setG(1);
        c2.setB(1);
        gl.glPushMatrix();
        gl.glTranslatef(0,0.2f,-0.1f);
        gl.glRotated(0, 1, 0, 0);
        gl.glScaled(0.1f,0.1f,0.1f);
        c2.DibujarCuboI();
        gl.glPopMatrix();
        
        c1.setR(1);
        c1.setG(1);
        c1.setB(1);
        gl.glPushMatrix();
        gl.glTranslatef(0.2f,0,0);
        gl.glRotated(0, 1, 0, 0);
        gl.glScaled(0.1f,0.1f,0.1f);
        c1.DibujarCuboD();
        gl.glPopMatrix();
        
        c2.setR(1);
        c2.setG(1);
        c2.setB(1);
        gl.glPushMatrix();
        gl.glTranslatef(0.2f,0.2f,-0.1f);
        gl.glRotated(0, 1, 0, 0);
        gl.glScaled(0.1f,0.1f,0.1f);
        c2.DibujarCuboD();
        gl.glPopMatrix();
        
        //Cuerpo
        p2.setR(1);
        p2.setG(1);
        p2.setB(1);
        gl.glPushMatrix();
        gl.glTranslatef(0.1f,0.6f,-0.1f);
        gl.glRotated(0, 1, 0, 0);
        gl.glScaled(0.35f,0.3f,0.3f);
        p2.DibujarPiramide();
        gl.glPopMatrix();
        //Cuello
        c1.setR(0);
        c1.setG(0);
        c1.setB(0);
        gl.glPushMatrix();
        gl.glTranslatef(0.1f,0.9f,-0.1f);
        gl.glRotated(0, 1, 0, 0);
        gl.glScaled(0.07f,0.07f,0.03f);
        c1.DibujarCubo();
        gl.glPopMatrix();
        //Cabeza
        e.setR(0);
        e.setG(0);
        e.setB(0);
        gl.glPushMatrix();
        gl.glTranslatef(0.1f,1.1f,-0.1f);
        gl.glRotated(0, 1, 0, 0);
        gl.glScaled(0.2f,0.2f,0.2f);
        e.DibujaEsfera();
        gl.glPopMatrix();
        
        e.setR(1);
        e.setG(1);
        e.setB(1);
        gl.glPushMatrix();
        gl.glTranslatef(0.1f,1.1f,0f);
        gl.glRotated(0, 1, 0, 0);
        gl.glScaled(0.15f,0.15f,0.15f);
        e.DibujaEsfera();
        gl.glPopMatrix();
        
        //Sombrero
        con1.setR(1);
        con1.setG(1);
        con1.setB(1);
        gl.glPushMatrix();
        gl.glTranslatef(0.1f,1.2f,-0.1f);
        gl.glRotated(-40, 1, 0, 0);
        gl.glScaled(0.3f,0.3f,0.3f);
        con1.DibujaCono();
        gl.glPopMatrix();
        
        gl.glPopMatrix();
        
    }
    
    public void MovimientoPersonaje(){
        
        System.out.println("posicion en x: " +this.x);
        System.out.println("posicion en y: " +this.y);
        System.out.println("posicion en z: " +this.z);
        
        this.x+=1.7f;
        if(x>=-85){
            this.y+=0.3f;
        }
        if(x>=-50){
            this.y = -9;
        }
        if(x>=35){
            this.x=50;
            this.ry=50;
            ProyectoFinalCastillo.camz=10;
            this.z-=1f;   
        }
        if(z<=-57){
            ProyectoFinalCastillo.translay +=0.6f;
            System.out.println(ProyectoFinalCastillo.translay);
        }
        
        if (z<=-76){
            
            ProyectoFinalCastillo.translay=0;
            this.y=3f;
           // this.ry=90;
        }
        if(z<=-100){
            this.z=-140;
            this.ry=90;
            this.x +=2f;
            System.out.println("tx " + ProyectoFinalCastillo.translax);
        }
        if(ProyectoFinalCastillo.translax ==40){
            ProyectoFinalCastillo.vistaz=0;
            ProyectoFinalCastillo.translax=2f;
            this.x=90f;
            
        }
        
        
    }

}
