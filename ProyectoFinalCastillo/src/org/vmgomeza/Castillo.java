

package org.vmgomeza;

import Figuras.*;
import javax.media.opengl.GL;
import com.sun.opengl.util.GLUT;
import javax.media.opengl.glu.GLU;



public class Castillo {
    
    GL gl;
    GLU glu;
    GLUT glut;
    float x, y, z;
    float ancho, alto, profundidad;
    static float rx,ry,rz;
    Pasillo pasillo;
    Torre torre;
    EntradaCastillo entrada;
    Balcon balcon;
    Cuadrado cuadrado;
    Faro faro;
    Interior in;
    
    

    public Castillo(GL gl, GLUT glut,GLU glu,float x, float y, float z, float ancho, float alto, float profundidad,float rx,float ry,float rz) {
        this.gl = gl;
        this.glut = glut;
        this.glu = glu;
        this.x = x;
        this.y = y;
        this.z = z;
        this.ancho = ancho;
        this.alto = alto;
        this.profundidad = profundidad;
        this.rx = rx;
        this.ry = ry;
        this.rz = rz;
        this.pasillo = new Pasillo(gl);
        this.torre = new Torre(gl, glut);
        this.entrada = new EntradaCastillo(gl,glu);
        this.balcon = new Balcon(gl);
        this.cuadrado = new Cuadrado(gl, -1.4f, -0.3f, -5.6f, 6f, 6.8f, 90, 0, 0, 0.0272f,0.5494f,0.68f);
        this.faro = new Faro(gl);
        this.in = new Interior(gl,glut);
        
                
    }
    
    public void DibujarCastillo(){
        
        gl.glPushMatrix();
        
        gl.glTranslatef(this.x, this.y, this.z);
        gl.glRotatef(rx, 1, 0, 0);
        gl.glRotatef(ry, 0, 1, 0);
        gl.glRotatef(rz, 0, 0, 1);
       // if (ProyectoFinalCastillo.numcam ==3){
             gl.glRotatef(ProyectoFinalCastillo.rotary, 0, 1, 0);
             gl.glRotatef(ProyectoFinalCastillo.rotarx, 1, 0, 0); 
       // }
        gl.glScalef(this.ancho, this.alto, this.profundidad);
        
        torre.DibujarTorre();
        pasillo.DibujarPasillo();
        Torre1();
        Techo();
        entrada.DibujarEntrada();
        balcon.DibujarBalcon();
        gl.glPopMatrix();
        
    
}
    
    public void Torre1(){
        
        
        cuadrado.DibujarCuadrado();
        
           for (float i = -0.3f; i < 1.2f; i+=0.4) {
          gl.glPushMatrix();
          gl.glTranslated(0,i, 0);
          Paredes();
          gl.glPopMatrix();
        }
           
           in.DibujarInterior();
//           
        for (float i = 1.3f; i < 2.4f; i+=0.4) {
          gl.glPushMatrix();
          if (i==1.3f){
              gl.glColor3f(1, 1, 1);
          } else {
              gl.glColor3f(0.2686f, 0.6857f, 0.79f);
          }
          gl.glTranslated(0,i, 0);
          Paredes2();
          gl.glPopMatrix();
        }
    }
    
    
    public void Paredes(){
        
         gl.glColor3f(0.2686f, 0.6857f, 0.79f);
         //Parte frontal Derecha
        for (float i = 1f; i < 3.8; i+=0.4) {
            if(i==1){
                gl.glColor3f(1, 1, 1);  
            }else{
                 gl.glColor3f(0.2686f, 0.6857f, 0.79f);
            }
        gl.glPushMatrix();
        gl.glTranslatef(i, 0, 0);
        gl.glRotatef(0, 0, 1, 0);
        gl.glScalef(0.4f, 0.4f, 0.4f);
        glut.glutSolidCube(1);
        gl.glPopMatrix();
        }
        
        //Parte frontal Izquierda
        for (float i = -3f ; i < -0.4f; i+=0.4) {
             if(i>=-0.8f){
                gl.glColor3f(1, 1, 1);  
            }else{
                 gl.glColor3f(0.2686f, 0.6857f, 0.79f);
            }
        gl.glPushMatrix();
        gl.glTranslatef(i, 0, 0);
        gl.glRotatef(0, 0, 1, 0);
        gl.glScalef(0.4f, 0.4f, 0.4f);
        glut.glutSolidCube(1);
        gl.glPopMatrix();
        }
        
         gl.glColor3f(0.2686f, 0.6857f, 0.79f);
//        Pared Izquierda
        for (float i = 0 ; i < 6f; i+=0.4) {
        gl.glPushMatrix();
        gl.glTranslatef(-3f, 0, -1*i);
        gl.glRotatef(0, 0, 1, 0);
        gl.glScalef(0.4f, 0.4f, 0.4f);
        glut.glutSolidCube(1);
        gl.glPopMatrix();
        }
        
//        Pared Derecha delantera
        for (float i = 0 ; i < 3.8f; i+=0.4) {
             if(i>=3.6f){
                gl.glColor3f(1, 1, 1);  
            }else{
                 gl.glColor3f(0.2686f, 0.6857f, 0.79f);
            }
        gl.glPushMatrix();
        gl.glTranslatef(3.8f, 0, -1*i);
        gl.glRotatef(0, 0, 1, 0);
        gl.glScalef(0.4f, 0.4f, 0.4f);
        glut.glutSolidCube(1);
        gl.glPopMatrix();
        }
        
//        Pared Derecha trasera
        for (float i = 5.2f ; i < 6f; i+=0.4) {
            if(i==5.2f){
                gl.glColor3f(1, 1, 1);  
            }else{
                 gl.glColor3f(0.2686f, 0.6857f, 0.79f);
            }
        gl.glPushMatrix();
        gl.glTranslatef(3.8f, 0, -1*i);
        gl.glRotatef(0, 0, 1, 0);
        gl.glScalef(0.4f, 0.4f, 0.4f);
        glut.glutSolidCube(1);
        gl.glPopMatrix();
        }
        gl.glColor3f(0.2686f, 0.6857f, 0.79f);
//        Pared Trasera
        for (float i = 0 ; i < 7.2f; i+=0.4) {
        gl.glPushMatrix();
        gl.glTranslatef(-3f+i, 0, -6);
        gl.glRotatef(0, 0, 1, 0);
        gl.glScalef(0.4f, 0.4f, 0.4f);
        glut.glutSolidCube(1);
        gl.glPopMatrix();
        }
    }
    
    public void Paredes2(){
        
         //Parte frontal 
        for (float i = 0 ; i < 7.2f; i+=0.4) {
        gl.glPushMatrix();
        gl.glTranslatef(-3f+i, 0, 0);
        gl.glRotatef(0, 0, 1, 0);
        gl.glScalef(0.4f, 0.4f, 0.4f);
        glut.glutSolidCube(1);
        gl.glPopMatrix();
        }
        
        //Pared Izquierda
        for (float i = 0 ; i < 6f; i+=0.4) {
        gl.glPushMatrix();
        gl.glTranslatef(-3f, 0, -1*i);
        gl.glRotatef(0, 0, 1, 0);
        gl.glScalef(0.4f, 0.4f, 0.4f);
        glut.glutSolidCube(1);
        gl.glPopMatrix();
        }
        
        //Pared Derecha
        for (float i = 0 ; i < 6f; i+=0.4) {
        gl.glPushMatrix();
        gl.glTranslatef(3.8f, 0, -1*i);
        gl.glRotatef(0, 0, 1, 0);
        gl.glScalef(0.4f, 0.4f, 0.4f);
        glut.glutSolidCube(1);
        gl.glPopMatrix();
        }
        
        //Pared Trasera
        for (float i = 0 ; i < 7.2f; i+=0.4) {
        gl.glPushMatrix();
        gl.glTranslatef(-3f+i, 0, -6);
        gl.glRotatef(0, 0, 1, 0);
        gl.glScalef(0.4f, 0.4f, 0.4f);
        glut.glutSolidCube(1);
        gl.glPopMatrix();
        }
    }
    
    public void Techo(){
        
        //Parte frontal
        gl.glColor3f(1f, 1f, 1f);
        for (float i = 0 ; i < 7.2f; i+=0.4) {
        gl.glPushMatrix();
        gl.glTranslatef(-3f+i, 2.2f, 0);
        gl.glRotatef(0, 0, 1, 0);
        gl.glScalef(0.2f, 0.2f, 0.2f);
        glut.glutSolidCube(1);
        gl.glPopMatrix();
        }
        for (float i = 0 ; i < 7.2f; i+=0.4) {
        gl.glPushMatrix();
        gl.glTranslatef(-3f+i, 2.4f, 0);
        gl.glRotatef(0, 0, 1, 0);
        gl.glScalef(0.2f, 0.2f, 0.2f);
        glut.glutSolidCube(1);
        gl.glPopMatrix();
        }
        
        //Pared Izquierda
        for (float i = 0 ; i < 6f; i+=0.4) {
        gl.glPushMatrix();
        gl.glTranslatef(-3.1f, 2.2f, -1*i);
        gl.glRotatef(0, 0, 1, 0);
        gl.glScalef(0.2f, 0.2f, 0.2f);
        glut.glutSolidCube(1);
        gl.glPopMatrix();
        }
        
        //Pared Derecha
        for (float i = 0 ; i < 6f; i+=0.4) {
        gl.glPushMatrix();
        gl.glTranslatef(3.9f, 2.2f, -1*i);
        gl.glRotatef(0, 0, 1, 0);
        gl.glScalef(0.2f, 0.2f, 0.2f);
        glut.glutSolidCube(1);
        gl.glPopMatrix();
        }
        
        //Pared Trasera
        for (float i = 0 ; i < 7.2f; i+=0.4) {
        gl.glPushMatrix();
        gl.glTranslatef(-3f+i, 2.2f, -6);
        gl.glRotatef(0, 0, 1, 0);
        gl.glScalef(0.2f, 0.2f, 0.2f);
        glut.glutSolidCube(1);
        gl.glPopMatrix();
        }  
        
        //Pared Izquierda
        for (float i = 0 ; i < 6f; i+=0.4) {
        gl.glPushMatrix();
        gl.glTranslatef(-3.1f, 2.4f, -1*i);
        gl.glRotatef(0, 0, 1, 0);
        gl.glScalef(0.2f, 0.2f, 0.2f);
        glut.glutSolidCube(1);
        gl.glPopMatrix();
        }
        
        //Pared Derecha
        for (float i = 0 ; i < 6f; i+=0.4) {
        gl.glPushMatrix();
        gl.glTranslatef(3.9f, 2.4f, -1*i);
        gl.glRotatef(0, 0, 1, 0);
        gl.glScalef(0.2f, 0.2f, 0.2f);
        glut.glutSolidCube(1);
        gl.glPopMatrix();
        }
        
        //Pared Trasera
        for (float i = 0 ; i < 7.2f; i+=0.4) {
        gl.glPushMatrix();
        gl.glTranslatef(-3f+i, 2.4f, -6);
        gl.glRotatef(0, 0, 1, 0);
        gl.glScalef(0.2f, 0.2f, 0.2f);
        glut.glutSolidCube(1);
        gl.glPopMatrix();
        }  
    }
    
}
