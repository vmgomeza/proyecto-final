/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.vmgomeza;
import Figuras.*;
import javax.media.opengl.GL;

  
public class Balcon {
    Cubo cubo,cuboc;
    GL gl;

    public Balcon(GL gl) {
        this.gl = gl;
        this.cubo = new Cubo(gl,0.5f, 2, 3,0,90,0);
        this.cuboc = new Cubo(gl, 1, 0.5f, 1, 0, 90, 0);
    }
    
    public void DibujarBalcon(){
    
        pisos();
        columnas();
        
        
}

    public void pisos(){
        cubo.setR(0.2513f);
        cubo.setG(0.0319f);
        cubo.setB(0.29f);
        
        //suelo
        for (float j = 0; j < 2.2; j+=0.4) {
            for (float i = 0f; i < 4.2f; i+=0.4) {
        gl.glPushMatrix();
        gl.glTranslatef(3.6f+j, -0.8f, 17 + i);
        gl.glRotatef(0, 0, 1, 0);
        gl.glScalef(0.2f, 0.2f, 0.2f);
        cubo.DibujarCubo();
        gl.glPopMatrix();
          }
        }
        
        //techo
        for (float j = 0; j < 2.2; j+=0.4) {
            for (float i = 0f; i < 4.2f; i+=0.4) {
        gl.glPushMatrix();
        gl.glTranslatef(3.6f+j, 3f, 17 + i);
        gl.glRotatef(0, 0, 1, 0);
        gl.glScalef(0.2f, 0.2f, 0.2f);
        cubo.DibujarCubo();
        gl.glPopMatrix();
          }
        }
        
        for (float j = 0; j < 2.2; j+=0.4) {
            for (float i = 0f; i < 4.2f; i+=0.4) {
        gl.glPushMatrix();
        gl.glTranslatef(3.6f+j, 3.4f, 17 + i);
        gl.glRotatef(0, 0, 1, 0);
        gl.glScalef(0.2f, 0.2f, 0.2f);
        cubo.DibujarCubo();
        gl.glPopMatrix();
          }
        }

    }
    
    public void columnas(){
        
       
        
        cuboc.setR(0.2686f);
        cuboc.setG(0.6857f);
        cuboc.setB(0.79f);
        //Columna superior izquierda
        for (float i = 0f; i < 3.8f; i+=0.1) {
        gl.glPushMatrix();
        gl.glTranslatef(3.4f, -0.6f+i,17.2f);
        gl.glRotatef(0, 0, 1, 0);
        gl.glScalef(0.2f, 0.2f, 0.2f);
        cuboc.DibujarCubo2();
        gl.glPopMatrix();
          }
        
        //Columna superior derecha
        for (float i = 0f; i < 3.8f; i+=0.1) {
        gl.glPushMatrix();
        gl.glTranslatef(5.8f, -0.6f+i,17.2f);
        gl.glRotatef(0, 0, 1, 0);
        gl.glScalef(0.2f, 0.2f, 0.2f);
        cuboc.DibujarCubo2();
        gl.glPopMatrix();
          }
        
        //Columna inferior izquierda
        for (float i = 0f; i < 3.8f; i+=0.1) {
        gl.glPushMatrix();
        gl.glTranslatef(3.4f, -0.6f+i,21.2f);
        gl.glRotatef(0, 0, 1, 0);
        gl.glScalef(0.2f, 0.2f, 0.2f);
        cuboc.DibujarCubo2();
        gl.glPopMatrix();
          }
        
        //Columna inferior derecha
        for (float i = 0f; i < 3.8f; i+=0.1) {
        gl.glPushMatrix();
        gl.glTranslatef(5.8f, -0.6f+i,21.2f);
        gl.glRotatef(0, 0, 1, 0);
        gl.glScalef(0.2f, 0.2f, 0.2f);
        cuboc.DibujarCubo2();
        gl.glPopMatrix();
          }
          
    }
    
}
