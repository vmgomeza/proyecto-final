/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.vmgomeza;

import Figuras.*;
import javax.media.opengl.GL;
import javax.media.opengl.glu.GLU;
import javax.media.opengl.glu.GLUquadric;

public class EntradaCastillo {
    
        Cubo cubo2,pas,ca;
        GL gl;
        Faro faro;
        DiscoParcial Dp;
        GLU glu;
        GLUquadric quad;

    public EntradaCastillo(GL gl,GLU glu) {
        this.gl = gl;
        this.cubo2 = new Cubo(gl, 0.5f, 1, 3, 0, 90, 0);
        this.pas = new Cubo(gl, 0.5f, 0.5f, 0.5f, 0, 90, 0);
        this.faro = new Faro(gl);
        this.Dp = new DiscoParcial(gl, glu, quad, 6, 1.85f, 1.5f, 180, 90, 0);
        this.ca = new Cubo(gl, 1, 1f,0.2f, 0, 90, 0);
    }
      
    public void DibujarEntrada(){
        
        cubo2.setR(0.2686f);
        cubo2.setG(0.6857f);
        cubo2.setB(0.79f);
        
        //Gradas Descendientes
        for (float i = 0.2f; i < 1.4f; i+=0.2) {
        gl.glPushMatrix();
        gl.glTranslatef(0.2f, -0.2f+(-1)*i, 0.4f+i);
        gl.glRotatef(0, 0, 1, 0);
        gl.glScalef(0.2f, 0.2f, 1f);
        cubo2.DibujarCubo();
        gl.glPopMatrix();
        }
         for (float i = 0f; i < 7f; i+=0.2) {
        gl.glPushMatrix();
        gl.glTranslatef(0.2f, -1.4f, 1.6f+i);
        gl.glRotatef(0, 0, 1, 0);
        gl.glScalef(0.2f, 0.2f, 1f);
        cubo2.DibujarCubo();
        gl.glPopMatrix();
         }
         
          for (float i = 0f; i < 7f; i+=0.2) {
        gl.glPushMatrix();
        gl.glTranslatef(0.2f+(-1)*i, -1.4f, 8.6f);
        gl.glRotatef(0, 0, 1, 0);
        gl.glScalef(0.2f, 0.2f, 1.6f);
        cubo2.DibujarCubo();
        gl.glPopMatrix();
         }
         
          for (float i = 0f; i < 2f; i+=0.3) {
        gl.glPushMatrix();
        gl.glTranslatef(-7f+(-1)*i, -1.4f+(-1)*i, 8.6f);
        gl.glRotatef(0, 0, 1, 0);
        gl.glScalef(0.2f, 0.2f, 1.6f);
        cubo2.DibujarCubo();
        gl.glPopMatrix();
         }
          
           for (float i = 0f; i < 3f; i+=0.3) {
        gl.glPushMatrix();
        gl.glTranslatef(-8.8f+(-1)*i,-3.2f , 8.6f);
        gl.glRotatef(0, 0, 1, 0);
        gl.glScalef(0.2f, 0.2f, 1.6f);
        cubo2.DibujarCubo();
        gl.glPopMatrix();
         }
            Pasillo();
            Faroles();
            Arco();

    }

    public void Pasillo(){
          pas.setR(0.684f);
          pas.setG(0.8968f);
          pas.setB(0.95f);
          
          //Pasillo1
          //Izquierdo
          for (float i = 0f; i < 7; i+=0.2) {
        gl.glPushMatrix();
        gl.glTranslatef(0.7f, -1.2f, 2.6f+i);
        gl.glRotatef(0, 0, 1, 0);
        gl.glScalef(0.2f, 0.2f, 0.5f);
        pas.DibujarCuboD();
        gl.glPopMatrix();
         }
          //Derecho
          for (float i = 0f; i < 4.6; i+=0.2) {
        gl.glPushMatrix();
        gl.glTranslatef(-0.3f, -1.2f,2.6f+i);
        gl.glRotatef(0, 0, 1, 0);
        gl.glScalef(0.2f, 0.2f, 0.5f);
        pas.DibujarCuboI();
        gl.glPopMatrix();
         }
         //Pasillo2 
        //Pasillo Izquierdo
          for (float i = 0f; i < 7f; i+=0.2) {
        gl.glPushMatrix();
        gl.glTranslatef(-0.3f+(-1)*i, -1.2f, 7.2f);
        gl.glRotatef(0, 0, 1, 0);
        gl.glScalef(0.2f, 0.2f, 0.5f);
        pas.DibujarCuboT();
        gl.glPopMatrix();
         }
          //Pasillo Derecho
          for (float i = 0f; i < 8f; i+=0.2) {
        gl.glPushMatrix();
        gl.glTranslatef(0.7f+(-1)*i, -1.2f, 9.9f);
        gl.glRotatef(0, 0, 1, 0);
        gl.glScalef(0.2f, 0.2f, 0.5f);
        pas.DibujarCuboF();
        gl.glPopMatrix();
         }
          
        }
        
    public void Faroles(){
        
            //Faroles Izquierda
            for (float i = 0; i < 8; i+=1.2f) {
                gl.glPushMatrix();
                gl.glTranslatef(0.7f + (-1)*i,-0.8f,9.9f);
                gl.glRotatef(270, 0, 1, 0);
                gl.glScalef(0.5f, 0.5f, 0.5f);
                faro.dibujarFaro();
                gl.glPopMatrix();
            }
            //Faroles derecha
            for (float i = 0; i < 7; i+=1.2f) {
                gl.glPushMatrix();
                gl.glTranslatef(-1f + (-1)*i,-0.8f,7.2f);
                gl.glRotatef(270, 0, 1, 0);
                gl.glScalef(0.5f, 0.5f, 0.5f);
                faro.dibujarFaro();
                gl.glPopMatrix();
            }
         
        }
    
    public void Arco(){
        
        ca.setR(0.6915f);
        ca.setG(0.48f);
        ca.setB(0.75f);
        //Columna izquierda
        for (float i = 0f; i < 1.6f; i+=0.1) {
        gl.glPushMatrix();
        gl.glTranslatef(-0.3f,-1.1f+i,7.1f);
        gl.glRotatef(0, 0, 1, 0);
        gl.glScalef(0.1f, 0.1f, 0.1f);
        ca.DibujarCubo2();
        gl.glPopMatrix();
          }
        //Columna derecha
        for (float i = 0f; i < 1.6f; i+=0.1) {
        gl.glPushMatrix();
        gl.glTranslatef(0.7f,-1.1f+i,7.1f);
        gl.glRotatef(0, 0, 1, 0);
        gl.glScalef(0.1f, 0.1f, 0.1f);
        ca.DibujarCubo2();
        gl.glPopMatrix();
          }
        //Techo
        for (float i = 0f; i < 1.05f; i+=0.05) {
        gl.glPushMatrix();
        gl.glTranslatef(-0.3f+i,0.45f,7.1f);
        gl.glRotatef(0, 0, 1, 0);
        gl.glScalef(0.2f, 0.1f, 0.1f);
        ca.DibujarCubo2();
        gl.glPopMatrix();
        }
        for (float i = 0f; i < 1.05f; i+=0.1) {
        gl.glPushMatrix();
        gl.glTranslatef(-0.3f+i,0.55f,7.1f);
        gl.glRotatef(0, 0, 1, 0);
        gl.glScalef(0.2f, 0.1f, 0.1f);
        ca.DibujarCubo2();
        gl.glPopMatrix();
        }
        
         for (float i = 0; i < 0.25f; i+=0.01f) {
                Dp.setR(1f-i);
                Dp.setG(0.9904f-i);
                Dp.setB(0.54f-i);
                gl.glPushMatrix();
                gl.glTranslatef(0.2f,-1.1f,7.25f+ (-1)*i);
                gl.glRotatef(270, 0, 1, 0);
                gl.glScalef(0.3f, 0.3f, 0.3f);
                Dp.DibujaDisco();
                gl.glPopMatrix();
            }
         
         
         
    }
    
}
