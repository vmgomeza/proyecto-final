
package org.vmgomeza;
import Figuras.*;
import javax.media.opengl.GL;

/**
 *
 * @author Vale
 */
public class Pasillo {
    
    Cubo cubo;
    GL gl;
    Barandal barandal;

    public Pasillo(GL gl) {
        this.gl = gl;
        this.barandal = new Barandal(gl);
        this.cubo = new Cubo(gl,0.5f, 2, 3,0,90,0);
    }
    
    
    public void DibujarPasillo(){
        
        cubo.setR(0.2513f);
        cubo.setG(0.0319f);
        cubo.setB(0.29f);
        //Gradas Ascendientes
        float m = -4.6f;
        for (float j = -0.2f; j < 1.2; j+=0.2f) {
            gl.glPushMatrix();
            if (j==0.0f){
                m=-3.4f;
            } else if(j==0.2f ){
                m=-2.8f;
            }else if(j==0.4f){
                m=-2f;
            }else if(j==0.6f){
                m=-1.2f;
            }else if(j==0.8f){
                m=-0.6f;
            }else if(j== 1.0f){
                m=0f;
            }             
            
        for (float i = 0f; i < 2.8f; i+=0.4) {
        gl.glPushMatrix();
        gl.glTranslatef(4.6f, j, m + i);
        gl.glRotatef(0, 0, 1, 0);
        gl.glScalef(0.2f, 0.2f, 0.2f);
        cubo.DibujarCubo();
        gl.glPopMatrix();
        }
           gl.glPopMatrix();
        }
        
        //Pasillo
         for (float i = 0f; i < 6.8f; i+=0.4) {
        gl.glPushMatrix();
        gl.glTranslatef(4.6f, 1.2f, 0.4f + i);
        gl.glRotatef(0, 0, 1, 0);
        gl.glScalef(0.2f, 0.4f, 0.2f);
        cubo.DibujarCubo();
        gl.glPopMatrix();
        }

       //Gradas Descendientes
        float n = 4.8f;
        for (float j = 1.2f; j > 0.6f; j-=0.2f) {
            
            gl.glPushMatrix();
            if (j==1.0f){
                n=5.4f;
            } else if(j==0.8f ){
                 n=6f;
            } 
        for (float i = 0f; i < 2.8f; i+=0.4) {
        gl.glPushMatrix();
        gl.glTranslatef(4.6f, j, n + i);
        gl.glRotatef(0, 0, 1, 0);
        gl.glScalef(0.2f, 0.2f, 0.2f);
        cubo.DibujarCubo();
        gl.glPopMatrix();
        }
           gl.glPopMatrix();
        }
        
        //Gradas Descendientes2
         for (float i = 0f; i < 2.8f; i+=0.4) {
        gl.glPushMatrix();
        gl.glTranslatef(4.6f, 0.6f, 6.4f + i);
        gl.glRotatef(0, 0, 1, 0);
        gl.glScalef(0.2f, 0.2f, 0.2f);
        cubo.DibujarCubo();
        gl.glPopMatrix();
        } 
        for (float i = 0f; i < 2.8f; i+=0.4) {
        gl.glPushMatrix();
        gl.glTranslatef(4.6f, 0.4f, 6.8f + i);
        gl.glRotatef(0, 0, 1, 0);
        gl.glScalef(0.2f, 0.2f, 0.2f);
        cubo.DibujarCubo();
        gl.glPopMatrix();
        }
        for (float i = 0f; i < 2.8f; i+=0.4) {
        gl.glPushMatrix();
        gl.glTranslatef(4.6f, 0.2f, 7.2f + i);
        gl.glRotatef(0, 0, 1, 0);
        gl.glScalef(0.2f, 0.2f, 0.2f);
        cubo.DibujarCubo();
        gl.glPopMatrix();
        }
//        
        //Pasillo2
        for (float i = 0f; i < 4.8f; i+=0.4) {
        gl.glPushMatrix();
        gl.glTranslatef(4.6f, 0f, 7.8f + i);
        gl.glRotatef(0, 0, 1, 0);
        gl.glScalef(0.2f, 0.4f, 0.2f);
        cubo.DibujarCubo();
        gl.glPopMatrix();
        }
        
        //PasilloLateral
        for (float j = 0f; j < 0.8f; j+=0.4) {
        for (float i = 0f; i < 0.6f; i+=0.4) {
        gl.glPushMatrix();
        gl.glTranslatef(5.8f, -0.2f, 11.1f+j);
        gl.glRotatef(0, 0, 1, 0);
        gl.glScalef(0.2f, 0.1f, 0.2f);
        cubo.DibujarCubo();
        gl.glPopMatrix();
        }
        }
//        
        //Gradas Descendientes
        for (float i = 0f; i < 0.8f; i+=0.4) {
        gl.glPushMatrix();
        gl.glTranslatef(4.6f, -0.2f, 12.4f + i);
        gl.glRotatef(0, 0, 1, 0);
        gl.glScalef(0.2f, 0.2f, 0.2f);
        cubo.DibujarCubo();
        gl.glPopMatrix();
        }
         for (float i = 0f; i < 0.8f; i+=0.4) {
        gl.glPushMatrix();
        gl.glTranslatef(4.6f, -0.4f, 12.8f + i);
        gl.glRotatef(0, 0, 1, 0);
        gl.glScalef(0.2f, 0.2f, 0.2f);
        cubo.DibujarCubo();
        gl.glPopMatrix();
        }
          for (float i = 0f; i < 0.8f; i+=0.4) {
        gl.glPushMatrix();
        gl.glTranslatef(4.6f, -0.6f, 13.2f + i);
        gl.glRotatef(0, 0, 1, 0);
        gl.glScalef(0.2f, 0.2f, 0.2f);
        cubo.DibujarCubo();
        gl.glPopMatrix();
        }
           for (float i = 0f; i < 3.8f; i+=0.4) {
        gl.glPushMatrix();
        gl.glTranslatef(4.6f, -0.8f, 13.6f + i);
        gl.glRotatef(0, 0, 1, 0);
        gl.glScalef(0.2f, 0.2f, 0.2f);
        cubo.DibujarCubo();
        gl.glPopMatrix();
        } 
    
        Barandales();
}
    
    public void Barandales(){
        
        //1eras Gradas Ascendientes
        gl.glPushMatrix();
        gl.glTranslatef(5.1f,1.3f,-0.4f);
        gl.glRotatef(270, 0, 1, 0);
        gl.glScalef(0.2f, 0.2f, 0.2f);
        barandal.dibujarBarandal();
        gl.glPopMatrix();
        gl.glPushMatrix();
        gl.glTranslatef(5.1f,1.1f,-1f);
        gl.glRotatef(270, 0, 1, 0);
        gl.glScalef(0.29f, 0.2f, 0.2f);
        barandal.dibujarBarandal();
        gl.glPopMatrix();
        gl.glPushMatrix();
        gl.glTranslatef(5.1f,0.9f,-1.6f);
        gl.glRotatef(270, 0, 1, 0);
        gl.glScalef(0.3f, 0.2f, 0.2f);
        barandal.dibujarBarandal();
        gl.glPopMatrix();
        gl.glPushMatrix();
        gl.glTranslatef(5.1f,0.7f,-2.3f);
        gl.glRotatef(270, 0, 1, 0);
        gl.glScalef(0.35f, 0.2f, 0.2f);
        barandal.dibujarBarandal();
        gl.glPopMatrix();
        gl.glPushMatrix();
        gl.glTranslatef(5.1f,0.5f,-3.1f);
        gl.glRotatef(270, 0, 1, 0);
        gl.glScalef(0.4f, 0.2f, 0.2f);
        barandal.dibujarBarandal();
        gl.glPopMatrix();
        gl.glPushMatrix();
        gl.glTranslatef(5.1f,0.3f,-3.7f);
        gl.glRotatef(270, 0, 1, 0);
        gl.glScalef(0.3f, 0.2f, 0.2f);
        barandal.dibujarBarandal();
        gl.glPopMatrix();
        //Piso
        gl.glPushMatrix();
        gl.glTranslatef(5.1f,0.1f,-4.8f);
        gl.glRotatef(270, 0, 1, 0);
        gl.glScalef(0.65f, 0.2f, 0.2f);
        barandal.dibujarBarandal();
        gl.glPopMatrix();
        gl.glPushMatrix();
        gl.glTranslatef(5f,0.1f,-4.9f);
        gl.glRotatef(180, 0, 1, 0);
        gl.glScalef(0.6f, 0.2f, 0.2f);
        barandal.dibujarBarandal();
        gl.glPopMatrix();
        
     
        //Primer pasillo
        //Barandal Izquierdo
         for (float i = 0f; i < 6.8f; i+=0.4) {
        gl.glPushMatrix();
        gl.glTranslatef(4.1f,1.6f,i);
        gl.glRotatef(270, 0, 1, 0);
        gl.glScalef(0.2f, 0.2f, 0.2f);
        barandal.dibujarBarandal();
        gl.glPopMatrix();
        }
        gl.glPushMatrix();
        gl.glTranslatef(4.1f,1.6f,6.8f);
        gl.glRotatef(270, 0, 1, 0);
        gl.glScalef(0.2f, 0.2f, 0.2f);
        barandal.dibujarBarandalFinal();
        gl.glPopMatrix();
         //Barandal Derecho
         for (float i = 0f; i < 6.8f; i+=0.4) {
        gl.glPushMatrix();
        gl.glTranslatef(5.1f,1.6f,i);
        gl.glRotatef(270, 0, 1, 0);
        gl.glScalef(0.2f, 0.2f, 0.2f);
        barandal.dibujarBarandal();
        gl.glPopMatrix();
        }
        gl.glPushMatrix();
        gl.glTranslatef(5.1f,1.6f,6.8f);
        gl.glRotatef(270, 0, 1, 0);
        gl.glScalef(0.2f, 0.2f, 0.2f);
        barandal.dibujarBarandalFinal();
        gl.glPopMatrix();
        //Gradas Descendientes
        //Barandal derecho
        gl.glPushMatrix();
        gl.glTranslatef(5.1f, 1.5f, 7.6f);
        gl.glRotatef(90, 0, 1, 0);
        gl.glScalef(0.2f, 0.2f, 0.2f);
        barandal.dibujarBarandal();
        gl.glPopMatrix();
        gl.glPushMatrix();
        gl.glTranslatef(5.1f, 1.3f, 8.2f);
        gl.glRotatef(90, 0, 1, 0);
        gl.glScalef(0.3f, 0.2f, 0.2f);
        barandal.dibujarBarandal();
        gl.glPopMatrix();
        gl.glPushMatrix();
        gl.glTranslatef(5.1f, 1.1f, 8.8f);
        gl.glRotatef(90, 0, 1, 0);
        gl.glScalef(0.3f, 0.2f, 0.2f);
        barandal.dibujarBarandal();
        gl.glPopMatrix();
         gl.glPushMatrix();
        gl.glTranslatef(5.1f, 0.9f, 9.2f);
        gl.glRotatef(90, 0, 1, 0);
        gl.glScalef(0.2f, 0.2f, 0.2f);
        barandal.dibujarBarandal();
        gl.glPopMatrix();
        gl.glPushMatrix();
        gl.glTranslatef(5.1f, 0.7f, 9.6f);
        gl.glRotatef(90, 0, 1, 0);
        gl.glScalef(0.2f, 0.2f, 0.2f);
        barandal.dibujarBarandal();
        gl.glPopMatrix();
        gl.glPushMatrix();
        gl.glTranslatef(5.1f, 0.5f, 10f);
        gl.glRotatef(90, 0, 1, 0);
        gl.glScalef(0.2f, 0.2f, 0.2f);
        barandal.dibujarBarandal();
        gl.glPopMatrix();
        //Barandal izquierdo
        gl.glPushMatrix();
        gl.glTranslatef(4.1f, 1.5f, 7.6f);
        gl.glRotatef(90, 0, 1, 0);
        gl.glScalef(0.2f, 0.2f, 0.2f);
        barandal.dibujarBarandal();
        gl.glPopMatrix();
        gl.glPushMatrix();
        gl.glTranslatef(4.1f, 1.3f, 8.2f);
        gl.glRotatef(90, 0, 1, 0);
        gl.glScalef(0.3f, 0.2f, 0.2f);
        barandal.dibujarBarandal();
        gl.glPopMatrix();
        gl.glPushMatrix();
        gl.glTranslatef(4.1f, 1.1f, 8.8f);
        gl.glRotatef(90, 0, 1, 0);
        gl.glScalef(0.3f, 0.2f, 0.2f);
        barandal.dibujarBarandal();
        gl.glPopMatrix();
        gl.glPushMatrix();
        gl.glTranslatef(4.1f, 0.9f, 9.2f);
        gl.glRotatef(90, 0, 1, 0);
        gl.glScalef(0.2f, 0.2f, 0.2f);
        barandal.dibujarBarandal();
        gl.glPopMatrix();
        gl.glPushMatrix();
        gl.glTranslatef(4.1f, 0.7f, 9.6f);
        gl.glRotatef(90, 0, 1, 0);
        gl.glScalef(0.2f, 0.2f, 0.2f);
        barandal.dibujarBarandal();
        gl.glPopMatrix();
        gl.glPushMatrix();
        gl.glTranslatef(4.1f, 0.5f, 10f);
        gl.glRotatef(90, 0, 1, 0);
        gl.glScalef(0.2f, 0.2f, 0.2f);
        barandal.dibujarBarandal();
        gl.glPopMatrix();
        
        //Pasillo2
        //Barandal Izquierdo
         for (float i = 0f; i < 2.2f; i+=0.4) {
        gl.glPushMatrix();
        gl.glTranslatef(4.1f,0.4f,10+i);
        gl.glRotatef(270, 0, 1, 0);
        gl.glScalef(0.2f, 0.2f, 0.2f);
        barandal.dibujarBarandal();
        gl.glPopMatrix();
        }
        gl.glPushMatrix();
        gl.glTranslatef(4.1f,0.4f,12f);
        gl.glRotatef(270, 0, 1, 0);
        gl.glScalef(0.2f, 0.2f, 0.2f);
        barandal.dibujarBarandalFinal();
        gl.glPopMatrix();
        //Barandal Derecho
        gl.glPushMatrix();
        gl.glTranslatef(5.1f,0.4f,10f);
        gl.glRotatef(270, 0, 1, 0);
        gl.glScalef(0.2f, 0.2f, 0.2f);
        barandal.dibujarBarandal();
        gl.glPopMatrix();
         gl.glPushMatrix();
        gl.glTranslatef(5.1f,0.4f,10.4f);
        gl.glRotatef(270, 0, 1, 0);
        gl.glScalef(0.2f, 0.2f, 0.2f);
        barandal.dibujarBarandalFinal();
        gl.glPopMatrix();
        gl.glPushMatrix();
        gl.glTranslatef(5.1f,0.4f,12.1f);
        gl.glRotatef(270, 0, 1, 0);
        gl.glScalef(0.2f, 0.2f, 0.2f);
        barandal.dibujarBarandalFinal();
        gl.glPopMatrix();
        
        //Gradas descendientes2
        //Derecha
        gl.glPushMatrix();
        gl.glTranslatef(5.1f,0.1f,12.8f);
        gl.glRotatef(90, 0, 1, 0);
        gl.glScalef(0.2f, 0.2f, 0.2f);
        barandal.dibujarBarandal();
        gl.glPopMatrix();
        gl.glPushMatrix();
        gl.glTranslatef(5.1f,0.1f,13.15f);
        gl.glRotatef(90, 0, 1, 0);
        gl.glScalef(0.2f, 0.2f, 0.2f);
        barandal.dibujarBarandal();
        gl.glPopMatrix();
        gl.glPushMatrix();
        gl.glTranslatef(5.1f,-0.1f,13.55f);
        gl.glRotatef(90, 0, 1, 0);
        gl.glScalef(0.2f, 0.2f, 0.2f);
        barandal.dibujarBarandal();
        gl.glPopMatrix();
        gl.glPushMatrix();
        gl.glTranslatef(5.1f,-0.3f,13.95f);
        gl.glRotatef(90, 0, 1, 0);
        gl.glScalef(0.2f, 0.2f, 0.2f);
        barandal.dibujarBarandal();
        gl.glPopMatrix();
        //Izquierda
        gl.glPushMatrix();
        gl.glTranslatef(4.1f,0.1f,12.8f);
        gl.glRotatef(90, 0, 1, 0);
        gl.glScalef(0.2f, 0.2f, 0.2f);
        barandal.dibujarBarandal();
        gl.glPopMatrix();
        gl.glPushMatrix();
        gl.glTranslatef(4.1f,0.1f,13.15f);
        gl.glRotatef(90, 0, 1, 0);
        gl.glScalef(0.2f, 0.2f, 0.2f);
        barandal.dibujarBarandal();
        gl.glPopMatrix();
        gl.glPushMatrix();
        gl.glTranslatef(4.1f,-0.1f,13.55f);
        gl.glRotatef(90, 0, 1, 0);
        gl.glScalef(0.2f, 0.2f, 0.2f);
        barandal.dibujarBarandal();
        gl.glPopMatrix();
        gl.glPushMatrix();
        gl.glTranslatef(4.1f,-0.3f,13.95f);
        gl.glRotatef(90, 0, 1, 0);
        gl.glScalef(0.2f, 0.2f, 0.2f);
        barandal.dibujarBarandal();
        gl.glPopMatrix();
        //Pasillo derecho
        gl.glPushMatrix();
        gl.glTranslatef(5.1f,-0.5f,14.35f);
        gl.glRotatef(90, 0, 1, 0);
        gl.glScalef(0.2f, 0.2f, 0.2f);
        barandal.dibujarBarandal();
        gl.glPopMatrix();
        for (float i = 0f; i < 2.6f; i+=0.4) {
        gl.glPushMatrix();
        gl.glTranslatef(5.1f,-0.5f,14.35f+i);
        gl.glRotatef(90, 0, 1, 0);
        gl.glScalef(0.2f, 0.2f, 0.2f);
        barandal.dibujarBarandal();
        gl.glPopMatrix();
        }
        //Pasillo izquierdo
        gl.glPushMatrix();
        gl.glTranslatef(4.1f,-0.5f,14.35f);
        gl.glRotatef(90, 0, 1, 0);
        gl.glScalef(0.2f, 0.2f, 0.2f);
        barandal.dibujarBarandal();
        gl.glPopMatrix();
        for (float i = 0f; i < 2.6f; i+=0.4) {
        gl.glPushMatrix();
        gl.glTranslatef(4.1f,-0.5f,14.35f+i);
        gl.glRotatef(90, 0, 1, 0);
        gl.glScalef(0.2f, 0.2f, 0.2f);
        barandal.dibujarBarandal();
        gl.glPopMatrix();
        }
         
         
         
    }
    
  
    
    
}
