/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.vmgomeza;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

/**
Clase que implementa los movimientos del teclado
 */
public class MovimientosTeclado implements KeyListener {
    

    public MovimientosTeclado() {
    }

  
    @Override
    public void keyTyped(KeyEvent e) {
         if (e.getKeyChar() == '1') {
            ProyectoFinalCastillo.numcam = 1;

        }
        if (e.getKeyChar() == '2') {
            ProyectoFinalCastillo.numcam = 2;

        }
        if (e.getKeyChar() == '3') {
            ProyectoFinalCastillo.numcam = 3;

        }
        if (e.getKeyChar() == '4') {
            ProyectoFinalCastillo.numcam = 4;

        }
        if (e.getKeyChar() == '5') {
            ProyectoFinalCastillo.numcam = 5;
        }
       
    }

    @Override
    public void keyPressed(KeyEvent e) {
        
        if (e.getKeyCode() == KeyEvent.VK_D) {
            ProyectoFinalCastillo.translax -= 0.1f;
            System.out.println("Movimiento en x" + ProyectoFinalCastillo.translax);
            
        }

        if (e.getKeyCode() == KeyEvent.VK_A) {
           ProyectoFinalCastillo.translax += 0.1f;
            System.out.println("Movimiento en x" + ProyectoFinalCastillo.translax);
        }

        if (e.getKeyCode() == KeyEvent.VK_W) {
            ProyectoFinalCastillo.translay += 0.1f;
            System.out.println("Movimiento en y" + ProyectoFinalCastillo.translay);
        }

        if (e.getKeyCode() == KeyEvent.VK_S) {
            ProyectoFinalCastillo.translay -= 0.1f;
            System.out.println("Movimiento en y" + ProyectoFinalCastillo.translay);
        }

        if (e.getKeyCode() == KeyEvent.VK_UP) {
            ProyectoFinalCastillo.translaz += 0.1f;
            //ProyectoFinalCastillo.translax += 1f;
            System.out.println("Movimiento en z" + ProyectoFinalCastillo.translaz);
        }

        if (e.getKeyCode() == KeyEvent.VK_DOWN) {
            ProyectoFinalCastillo.translaz -= 0.1f;
            System.out.println("Movimiento en z" + ProyectoFinalCastillo.translaz);
        }
        
        if (e.getKeyCode() == KeyEvent.VK_RIGHT){
            ProyectoFinalCastillo.rotary -= 1f;
            System.out.println("Rotacion en y" + ProyectoFinalCastillo.rotary);
        }
        if (e.getKeyCode() == KeyEvent.VK_LEFT) {
            ProyectoFinalCastillo.rotary += 1f;
            System.out.println("Rotacion en y" + ProyectoFinalCastillo.rotary);
        }
        
        if (e.getKeyCode() == KeyEvent.VK_Z) {
            ProyectoFinalCastillo.rotarx += 1f;
            System.out.println("Rotacion en x" + ProyectoFinalCastillo.rotarx);
        }
        if (e.getKeyCode() == KeyEvent.VK_X) {
            ProyectoFinalCastillo.rotarx -= 1f;
            System.out.println("Rotacion en x" + ProyectoFinalCastillo.rotarx);
        }
        
        if (e.getKeyCode() == KeyEvent.VK_J) {
            ProyectoFinalCastillo.r += 2f;
            System.out.println("Rotacion en x" + ProyectoFinalCastillo.rotarx);
        }
        
        if (e.getKeyCode() == KeyEvent.VK_K) {
            ProyectoFinalCastillo.r -= 2f;
            System.out.println("Rotacion en x" + ProyectoFinalCastillo.rotarx);
        }
        
        if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
            ProyectoFinalCastillo.rotarx=0;
            ProyectoFinalCastillo.rotary=0;
            ProyectoFinalCastillo.rotarz=0;
            ProyectoFinalCastillo.translax=0;
            ProyectoFinalCastillo.translay=0;
            ProyectoFinalCastillo.translaz=0;
            
        }
        
        
    }

   
    public void keyReleased(KeyEvent e) {
      
    }

   
}
