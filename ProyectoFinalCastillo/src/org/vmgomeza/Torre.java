/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.vmgomeza;

import Figuras.Cubo;
import com.sun.opengl.util.GLUT;
import javax.media.opengl.GL;

/**
 *
 * @author Vale
 */
public class Torre {
    Cubo cubo,gradas;
    GL gl;
    GLUT glut;
    float m=0;

    public Torre(GL gl, GLUT glut) {
        this.gl = gl;
        this.glut = glut;
        this.cubo = new Cubo(gl, 0.5f, 0.5f, 0.5f, 0, 0, 0);
        this.gradas = new Cubo(gl,0.5f,1,2.5f,0,90, 0);
    }
    
    public void DibujarTorre(){
       
        
        //Dibujar Suelo  
        cubo.setR(0.2513f);
        cubo.setG(0.0319f);
        cubo.setB(0.29f);
        for (float j = 0; j < 4.4f; j+=0.05) {
        for (float i = 0; i < 8.2f; i+=0.05) {
        gl.glPushMatrix();
        gl.glTranslatef(6.4f + j,-0.2f, 4.4f + i);
        gl.glRotatef(0, 0, 1, 0);
        gl.glScalef(0.2f, 0.1f, 0.2f);
        cubo.DibujarCubo();
        gl.glPopMatrix();
        }
        }
          
          SegndoPiso();
        ParedTrasera();
        ParedFrontal();
        ParedIzquierda();
        ParedDerecha();
            
        
    }
    
    public void ParedTrasera(){
        //Pared Trasera
        for (float j = -0.2f; j < 3f; j+=0.05) {
        for (float i = 0; i < 8.45f; i+=0.05) {
             if (j<1.25 && j>0.8){
                cubo.setR(1f);
                cubo.setG(1f);
                cubo.setB(1f);
            } else {
                cubo.setR(0.7853f);
                cubo.setG(0.2349f);
                cubo.setB(0.87f);
            }
        gl.glPushMatrix();
        gl.glTranslatef(11f ,j, 4.4f + i);
        gl.glRotatef(0, 0, 1, 0);
        gl.glScalef(0.2f, 0.1f, 0.2f);
        cubo.DibujarCuboT();
        gl.glPopMatrix();
        }
        }
        
        //Techo
                cubo.setR(1f);
                cubo.setG(1f);
                cubo.setB(1f);
                
        
        for (float i = 0; i < 8.45f; i+=0.5) {
        gl.glPushMatrix();
        gl.glTranslatef(11f ,3.15f, 4.4f + i);
        gl.glRotatef(0, 0, 1, 0);
        gl.glScalef(0.2f, 0.2f, 0.2f);
        cubo.DibujarCubo2();
        gl.glPopMatrix();
        }
        
    }
    
    public void ParedFrontal(){
        
        //Pared Frontal PI
        for (float j = -0.2f; j < 0.8f; j+=0.05) {
        for (float i = 0; i < 6.25f; i+=0.05) {
            if (i>6){
                cubo.setR(1f);
                cubo.setG(1f);
                cubo.setB(1f);
            } else {
                cubo.setR(0.7853f);
                cubo.setG(0.2349f);
                cubo.setB(0.87f);
            }
        gl.glPushMatrix();
        gl.glTranslatef(6.2f ,j, 4.4f + i);
        gl.glRotatef(0, 0, 1, 0);
        gl.glScalef(0.2f, 0.1f, 0.2f);
        cubo.DibujarCuboF();
        gl.glPopMatrix();
        }
        }
        //Pared Frontal PD
        for (float j = -0.2f; j < 0.8f; j+=0.05) {
        for (float i = 0; i < 0.85f; i+=0.05) {
            if (i<0.2){
                cubo.setR(1f);
                cubo.setG(1f);
                cubo.setB(1f);
            } else {
                cubo.setR(0.7853f);
                cubo.setG(0.2349f);
                cubo.setB(0.87f);
            }
        gl.glPushMatrix();
        gl.glTranslatef(6.2f ,j, 12f + i);
        gl.glRotatef(0, 0, 1, 0);
        gl.glScalef(0.2f, 0.1f, 0.2f);
        cubo.DibujarCuboF();
        gl.glPopMatrix();
        }
        }
        //Pared Superior PD
        cubo.setR(1f);
        cubo.setG(1f);
        cubo.setB(1f);
        for (float j = -0.2f; j < 0.2f; j+=0.05) {
        for (float i = 0; i < 8.25f; i+=0.05) {
        gl.glPushMatrix();
        gl.glTranslatef(6.2f ,1.05f+j, 4.4f + i);
        gl.glRotatef(0, 0, 1, 0);
        gl.glScalef(0.2f, 0.1f, 0.2f);
        cubo.DibujarCuboF();
        gl.glPopMatrix();
        }
        }
        
        cubo.setR(0.7853f);
        cubo.setG(0.2349f);
        cubo.setB(0.87f);
        for (float j = -0.2f; j < 1.75f; j+=0.05) {
        for (float i = 0; i < 8.25f; i+=0.05) {
        gl.glPushMatrix();
        gl.glTranslatef(6.2f ,1.30f+j, 4.4f + i);
        gl.glRotatef(0, 0, 1, 0);
        gl.glScalef(0.2f, 0.1f, 0.2f);
        cubo.DibujarCuboF();
        gl.glPopMatrix();
        }
        }
        
        //Techo
        cubo.setR(1f);
        cubo.setG(1f);
        cubo.setB(1f);
                
        for (float i = 0; i < 8.45f; i+=0.5) {
        gl.glPushMatrix();
        gl.glTranslatef(6.2f ,3.15f, 4.4f + i);
        gl.glRotatef(0, 0, 1, 0);
        gl.glScalef(0.2f, 0.2f, 0.2f);
        cubo.DibujarCubo2();
        gl.glPopMatrix();
        }
        
    }
    
    public void ParedIzquierda(){
         
        //Pared Izquierda
        for (float j = -0.2f; j < 3f; j+=0.05) {
        for (float i = 0; i < 4.4f; i+=0.05) {
            if (j<1.25 && j>0.8){
                cubo.setR(1f);
                cubo.setG(1f);
                cubo.setB(1f);
            } else {
                cubo.setR(0.7853f);
                cubo.setG(0.2349f);
                cubo.setB(0.87f);
            }
        gl.glPushMatrix();
        gl.glTranslatef(6.4f + i ,j, 4.4f);
        gl.glRotatef(0, 0, 1, 0);
        gl.glScalef(0.2f, 0.1f, 0.2f);
        cubo.DibujarCuboI();
        gl.glPopMatrix();
        }
        }
        
        //Techo
         cubo.setR(1f);
         cubo.setG(1f);
         cubo.setB(1f);
                
        for (float i = 0; i < 4.4f; i+=0.5) {
        gl.glPushMatrix();
        gl.glTranslatef(6.4f + i ,3.15f, 4.4f);
        gl.glRotatef(0, 0, 1, 0);
        gl.glScalef(0.2f, 0.2f, 0.2f);
        cubo.DibujarCubo2();
        gl.glPopMatrix();
        }
        
    }
    
    public void ParedDerecha(){
        
                //Pared Derecha 
        for (float j = -0.2f; j < 3.05f; j+=0.1) {
        for (float i = 0; i < 4.6f; i+=0.1) {
            if (j<1.25 && j>0.8){
                cubo.setR(1f);
                cubo.setG(1f);
                cubo.setB(1f);
            } else {
                cubo.setR(0.7853f);
                cubo.setG(0.2349f);
                cubo.setB(0.87f);
            }
        gl.glPushMatrix();
        gl.glTranslatef(6.2f + i ,j, 12.8f);
        gl.glRotatef(0, 0, 1, 0);
        gl.glScalef(0.2f, 0.1f, 0.2f);
        cubo.DibujarCuboD();
        gl.glPopMatrix();
        }
        }
        
        //Techo
         cubo.setR(1f);
         cubo.setG(1f);
         cubo.setB(1f);
                
        for (float i = 0; i < 4.6f; i+=0.5) {
        gl.glPushMatrix();
        gl.glTranslatef(6.2f + i ,3.15f, 12.8f);
        gl.glRotatef(0, 0, 1, 0);
        gl.glScalef(0.2f, 0.2f, 0.2f);
        cubo.DibujarCubo2();
        gl.glPopMatrix();
        }
        
    }
    
    public void SegndoPiso(){
        //Dibujar Suelo  
        cubo.setR(0.2513f);
        cubo.setG(0.0319f);
        cubo.setB(0.29f);
        for (float j = 0; j < 4.4f; j+=0.05) {
        for (float i = 2; i < 8.2f; i+=0.05) {
        gl.glPushMatrix();
        gl.glTranslatef(6.4f + j,2.2f, 4.4f + i);
        gl.glRotatef(0, 0, 1, 0);
        gl.glScalef(0.2f, 0.1f, 0.2f);
        cubo.DibujarCubo();
        gl.glPopMatrix();
        }
        }
        for (float i = 0; i < 1.8f; i+=0.05) {
        gl.glPushMatrix();
        gl.glTranslatef(10.6f,2.2f, 6.2f-i);
        gl.glRotatef(0, 0, 1, 0);
        gl.glScalef(0.2f, 0.1f, 0.2f);
        cubo.DibujarCubo();
        gl.glPopMatrix();
        }
        for (float i = 0; i < 1.8f; i+=0.05) {
        gl.glPushMatrix();
        gl.glTranslatef(10.8f,2.2f, 6.2f-i);
        gl.glRotatef(0, 0, 1, 0);
        gl.glScalef(0.2f, 0.1f, 0.2f);
        cubo.DibujarCubo();
        gl.glPopMatrix();
        }
                

        
        //Gradas
        gradas.setR(0.9669f);
        gradas.setG(0.882f);
        gradas.setB(0.98f);
        
        //Gradas Ascendientes
        //1era grada
        gl.glPushMatrix();
        gl.glTranslatef(7.4f, 0f,4.8f);
        gl.glRotatef(-45f, 0, 1, 0);
        gl.glScalef(0.2f, 0.2f, 0.2f);
        gradas.DibujarCubo();
        gl.glPopMatrix();
        
        gl.glPushMatrix();
        gl.glTranslatef(7.67f,0.2f,4.9f);
        gl.glRotatef(-49.1f, 0, 1, 0);
        gl.glScalef(0.2f, 0.2f, 0.2f);
        gradas.DibujarCubo();
        gl.glPopMatrix();
        //x+0.27 y+=0.2 ang+=8.1
        gl.glPushMatrix();
        gl.glTranslatef(7.94f,0.4f,5f);
        gl.glRotatef(-53.2f, 0, 1, 0);
        gl.glScalef(0.2f, 0.2f, 0.2f);
        gradas.DibujarCubo();
        gl.glPopMatrix();
        
        gl.glPushMatrix();
        gl.glTranslatef(8.21f,0.6f,5.1f);
        gl.glRotatef(-57.3f, 0, 1, 0);
        gl.glScalef(0.2f, 0.2f, 0.2f);
        gradas.DibujarCubo();
        gl.glPopMatrix();
        
        gl.glPushMatrix();
        gl.glTranslatef(8.48f,0.8f,5.2f);
        gl.glRotatef(-61.4f, 0, 1, 0);
        gl.glScalef(0.2f, 0.2f, 0.2f);
        gradas.DibujarCubo();
        gl.glPopMatrix();
      
        gl.glPushMatrix();
        gl.glTranslatef(8.75f,1f,5.3f);
        gl.glRotatef(-65.5f, 0, 1, 0);
        gl.glScalef(0.2f, 0.2f, 0.2f);
        gradas.DibujarCubo();
        gl.glPopMatrix();
     
        gl.glPushMatrix();
        gl.glTranslatef(9.02f,1.2f,5.4f);
        gl.glRotatef(-69.6f, 0, 1, 0);
        gl.glScalef(0.2f, 0.2f, 0.2f);
        gradas.DibujarCubo();
        gl.glPopMatrix();
        
        gl.glPushMatrix();
        gl.glTranslatef(9.29f,1.4f,5.5f);
        gl.glRotatef(-74.7f, 0, 1, 0);
        gl.glScalef(0.2f, 0.2f, 0.2f);
        gradas.DibujarCubo();
        gl.glPopMatrix();
        
        gl.glPushMatrix();
        gl.glTranslatef(9.56f,1.6f,5.6f);
        gl.glRotatef(-78.8f, 0, 1, 0);
        gl.glScalef(0.2f, 0.2f, 0.2f);
        gradas.DibujarCubo();
        gl.glPopMatrix();
        
        gl.glPushMatrix();
        gl.glTranslatef(9.83f,1.8f,5.7f);
        gl.glRotatef(-82.9f, 0, 1, 0);
        gl.glScalef(0.2f, 0.2f, 0.2f);
        gradas.DibujarCubo();
        gl.glPopMatrix();
        
        gl.glPushMatrix();
        gl.glTranslatef(10.1f,2f,5.8f);
        gl.glRotatef(-87f, 0, 1, 0);
        gl.glScalef(0.2f, 0.2f, 0.2f);
        gradas.DibujarCubo();
        gl.glPopMatrix();

//        Ultima grada
        gl.glPushMatrix();
        gl.glTranslatef(10.4f,2.2f,6f);
        gl.glRotatef(-90f, 0, 1, 0);
        gl.glScalef(0.2f, 0.2f, 0.2f);
        gradas.DibujarCubo();
        gl.glPopMatrix();

    }
}
