
package org.vmgomeza;

import Figuras.Cubo;
import javax.media.opengl.GL;


public class Barandal {
    
    GL gl;
    Cubo c1,c2;

    public Barandal(GL gl) {
        this.gl = gl;
        this.c1 = new Cubo(gl, 2, 0.3f, 0.3f, 90, 0, 0);
        this.c2 = new Cubo(gl, 0.3f, 1.85f, 0.3f, 90, 0, 0);
    }
    
    public void dibujarBarandal(){
        
        c1.setR(0.2686f);
        c1.setG(0.6857f);
        c1.setB(0.79f);
        c2.setR(0.684f);
        c2.setG(0.8968f);
        c2.setB(0.95f);
        gl.glPushMatrix();
        gl.glTranslatef(0,0,0);
        gl.glRotated(90, 1, 0, 0);
        gl.glScaled(0.5f,0.5f,0.5f);
        c1.DibujarCubo2();
        gl.glPopMatrix();

        gl.glPushMatrix();
        gl.glTranslatef(1f,-0.2f,0);
        gl.glRotated(90, 1, 0, 0);
        gl.glScaled(0.5f,0.5f,0.5f);
        c2.DibujarCubo2();
        gl.glPopMatrix();
        
        gl.glPushMatrix();
        gl.glTranslatef(1f,0.5f, 0f);
        gl.glRotated(90, 1, 0, 0);
        gl.glScaled(0.5f,0.5f,0.5f);
        c2.DibujarCubo2();
        gl.glPopMatrix();
        
                
    }
    
    public void dibujarBarandalFinal(){
        
        c1.setR(0.2686f);
        c1.setG(0.6857f);
        c1.setB(0.79f);
        c2.setR(0.684f);
        c2.setG(0.8968f);
        c2.setB(0.95f);
        gl.glPushMatrix();
        gl.glTranslatef(0,0,0);
        gl.glRotated(90, 1, 0, 0);
        gl.glScaled(0.5f,0.5f,0.5f);
        c1.DibujarCubo2();
        gl.glPopMatrix();

        gl.glPushMatrix();
        gl.glTranslatef(1.1f,-0.2f,0);
        gl.glRotated(90, 1, 0, 0);
        gl.glScaled(0.5f,0.5f,0.5f);
        c2.DibujarCubo2();
        gl.glPopMatrix();
        
        gl.glPushMatrix();
        gl.glTranslatef(1.1f,0.5f, 0f);
        gl.glRotated(90, 1, 0, 0);
        gl.glScaled(0.5f,0.5f,0.5f);
        c2.DibujarCubo2();
        gl.glPopMatrix();
        gl.glPushMatrix();
        gl.glTranslatef(2.2f,0, 0f);
        gl.glRotated(90, 1, 0, 0);
        gl.glScaled(0.5f,0.5f,0.5f);
        c1.DibujarCubo2();
        gl.glPopMatrix();
        
                
    }
    
    
    
}
